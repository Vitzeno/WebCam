package motion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import com.twilio.Twilio;

/**
 * This class handles authorisation to use Twilio API for messaging services
 * @author Mohamed
 *
 */
public class Authorisation 
{
	private static final String RES_LOC = "res/";
	
	private static String ACCOUNT_SID;
	private static String AUTH_TOKEN;
	
	/**
	 * This method initialise account details
	 * @return
	 */
	public void authoriseAccount(String fileName) 
	{
		readAuthorisationFile(fileName);
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}
	
	/**
	 * This method reads the file containing the authorisation data
	 * @param fileName
	 */
	private void readAuthorisationFile(String fileName) 
	{
		try (BufferedReader br = new BufferedReader(new FileReader(new File(RES_LOC + fileName + ".txt")))) 
		{
		    String line;
		    while ((line = br.readLine()) != null) 
		    {
		    	String[] parts = line.split(" ");
		    	
		    	if (line.startsWith("ACCOUNT_SID")) 
		    		ACCOUNT_SID = parts[1];
		    	else 
		    		AUTH_TOKEN = parts[1];
		    }
		} catch (FileNotFoundException e1) 
		{
			System.out.println("File not found");
			e1.printStackTrace();
		} catch (IOException e1) 
		{
			System.out.println("Counld not read file.");
			e1.printStackTrace();
		}
		
		System.out.println("SID: " + ACCOUNT_SID + " TOKEN: " + AUTH_TOKEN);
	}

}
