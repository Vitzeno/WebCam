package motion;

import java.io.IOException;

public class Main 
{
	private static WebCam webcam = new WebCam();
	
	public static void main(String[] args) 
	{
		run();
		
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void run() 
	{
		webcam.authorisePhone("Auth");
		if(webcam.isWebCamConnected()) 
			webcam.startMotionDetection();	
	}
}
