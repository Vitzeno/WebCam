package motion;

import java.util.Random;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 * This class handles all phone related activities such as sending messages.
 * @author Mohamed
 *
 */
public class Phone 
{	
	private static Authorisation auth = new Authorisation();
	private static String DEFAULT_MESSAGE = "Motion detected in room";
	private static String[] MESSAGES_ARRAY = {"Got invaders in your room.", "Better check out your room.", "ALERT: motion detected in room."};
	
	public Phone(String fileName) 
	{
		auth.authoriseAccount(fileName);
	}
	
	/**
	 * Sends a random message from a predefined list
	 */
	public void sendSnarkyMessage() 
	{
		Random rand = new Random();
		String content = MESSAGES_ARRAY[rand.nextInt(MESSAGES_ARRAY.length)];
		
		Message message = Message.creator(
		    	new PhoneNumber("whatsapp:+447903116538"),	//To number
		        new PhoneNumber("whatsapp:+14155238886"), 	//From number Twillio generated number
		        content).create();			//Message
		
		System.out.println("Message sent: " + message.getSid());
	}
	
	/**
	 * Sends the default message
	 */
	public void sendDefaultMessade() 
	{
		Message message = Message.creator(
		    	new PhoneNumber("whatsapp:+447903116538"),	//To number
		        new PhoneNumber("whatsapp:+14155238886"), 	//From number Twillio generated number
		        DEFAULT_MESSAGE).create();			//Message
		
		System.out.println("Message sent: " + message.getSid());
	}
	
	/**
	 * Sends text message of provided content
	 * @param content of message to be sent
	 */
	public void sendCustomMessage(String content) 
	{
		Message message = Message.creator(
		    	new PhoneNumber("whatsapp:+447903116538"),	//To number
		        new PhoneNumber("whatsapp:+14155238886"), 	//From number Twillio generated number
		        content).create();			//Message
		
		System.out.println("Message sent: " + message.getSid());
	}
	
}
