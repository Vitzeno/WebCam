package motion;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionDetector;
import com.github.sarxos.webcam.WebcamMotionEvent;
import com.github.sarxos.webcam.WebcamMotionListener;

/**
 * This class handles motion detection and web cam related activities.
 * @author Mohamed
 *
 */
public class WebCam implements WebcamMotionListener
{
	private static final String OUT_LOC = "out/";
	
	private static Phone phone;
	private long lastTime = 0;
	//Time between message sends in ms
	private static int SEND_INTERVAL = 30000;
	
	private int fileNum = 0;
	
	Webcam webcam = Webcam.getDefault();
	WebcamMotionDetector detector = new WebcamMotionDetector(Webcam.getDefault());
	
	public void authorisePhone(String fileName) 
	{
		phone = new Phone(fileName);
	}
	
	/**
	 * Checks if webcam is present and connect and returns a
	 * boolean value
	 * @return 
	 */
	public boolean isWebCamConnected() 
	{
		if (webcam != null) 
		{
			System.out.println("Webcam: " + webcam.getName());
			return true;
		}	
		else
		{
			System.out.println("No webcam detected");
			return false;
		}
	}
	
	/**
	 * Captures an image and saves it as the provided
	 * name
	 * @param fileName
	 */
	public void captureImage(String fileName) 
	{
		webcam.open();

		// get image
		BufferedImage image = webcam.getImage();

		// save image to PNG file
		try {
			ImageIO.write(image, "PNG", new File(fileName + fileNum+ ".PNG"));
			System.out.println("Image saved to: " + fileName + fileNum + ".PNG");
			fileNum++;
		} catch (IOException e) {
			System.out.println("Failed to capture");
			e.printStackTrace();
		}
	}
	
	/**
	 * Starts the motion detection,
	 * sets a 500ms interval
	 */
	public void startMotionDetection() 
	{
		detector.setInterval(500);
		detector.addMotionListener(this);
		detector.start();
	}
	
	/**
	 * Handles detection of motion, captures image and sends notification
	 */
	@Override
	public void motionDetected(WebcamMotionEvent arg0) 
	{
		System.out.println("Motion Detected");
		captureImage(OUT_LOC + "MotionDetected");
		sendNotification();
	}
	
	/**
	 * Sends a notification to phone, only sends every SEND_INTERVAL
	 */
	private void sendNotification()
	{
		long currentTime = System.currentTimeMillis();
		if(currentTime - lastTime > SEND_INTERVAL) 
		{
			System.out.println("Sending message");
			phone.sendCustomMessage("Motion detected in room.");
			lastTime = currentTime;
		}
		
	}
}
